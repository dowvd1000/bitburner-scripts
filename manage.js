/** @param {NS} ns */
export async function main(ns) {
    let servers = JSON.parse(ns.read("servers.txt"));
    for (let i = 0; i < servers.length; i++) {
        if (ns.hasRootAccess(servers[i]) == true) {
            ns.scriptKill("scripts/deploy.js", servers[i]);
            ns.scriptKill("scripts/grow.js", servers[i]);
            ns.scriptKill("scripts/hack.js", servers[i]);
            ns.scriptKill("scripts/weak.js", servers[i]);
            ns.scp(["scripts/deploy.js", "scripts/grow.js", "scripts/hack.js", "scripts/weak.js"], servers[i]);
            ns.exec("scripts/deploy.js", servers[i]);
        }
    }

    ns.tail();
    ns.setTitle("Manage");
    ns.clearLog();
    ns.disableLog("ALL");
    await ns.sleep(0);

    const colors = {
        black: "\u001b[30m",
        red: "\u001b[31m",
        green: "\u001b[32m",
        yellow: "\u001b[33m",
        blue: "\u001b[34m",
        magenta: "\u001b[35m",
        cyan: "\u001b[36m",
        white: "\u001b[37m",
        brightBlack: "\u001b[30;1m",
        brightRed: "\u001b[31;1m",
        brightGreen: "\u001b[32;1m",
        brightYellow: "\u001b[33;1m",
        brightBlue: "\u001b[34;1m",
        brightMagenta: "\u001b[35;1m",
        brightCyan: "\u001b[36;1m",
        brightWhite: "\u001b[37;1m",
        reset: "\u001b[0m"
    };

    let growmoney = 1;

    let memgrow = ns.getScriptRam("scripts/grow.js");
    let memweak = ns.getScriptRam("scripts/weak.js");
    let memhack = ns.getScriptRam("scripts/hack.js");

    let queue = 0
    let lastaction;
    let queueplace;

    while (true) {
        let servers = JSON.parse(ns.read("servers.txt"));
        let targets = JSON.parse(ns.read("hackservers.txt"));
        for (let i = 0; i < servers.length; i++) {
            if (ns.hasRootAccess(servers[i]) == true) {
                try {
                    let log = JSON.parse(ns.read("logs/" + servers[i] + "-log-v2.txt"));
                    if (ns.getHackingLevel() >= ns.getServerRequiredHackingLevel(servers[i]) && log[0] == "IDLE") {
                        let id = queue;
                        if (queue < targets.length) {
                            queue++;
                        } else {
                            queue = 0;
                            id = 0;
                        }
                        queueplace = id;
                        if (ns.getServerMoneyAvailable(targets[id]) >= ns.getServerMaxMoney(targets[id]) * growmoney) {
                            let threads = getFreeMemory(ns, servers[i], memhack)
                            ns.exec("scripts/hack.js", servers[i], threads, threads, targets[id]);
                            lastaction = "HACK: " + targets[id] + " ► ON: " + servers[i];
                        } else if (ns.getServerSecurityLevel(targets[id]) > (ns.getServerMinSecurityLevel(targets[id]) + 5)) {
                            let threads = getFreeMemory(ns, servers[i], memweak)
                            ns.exec("scripts/weak.js", servers[i], threads, threads, targets[id]);
                            lastaction = "WEAKEN: " + targets[id] + " ► ON: " + servers[i];
                        } else if (ns.getServerMoneyAvailable(targets[id]) < ns.getServerMaxMoney(targets[id]) * growmoney) {
                            let threads = getFreeMemory(ns, servers[i], memgrow)
                            ns.exec("scripts/grow.js", servers[i], threads, threads, targets[id]);
                            lastaction = "GROW: " + targets[id] + " ► ON: " + servers[i];
                        }
                    }
                } catch (error) {
                    if (ns.fileExists("hostname.txt", servers[i]) == false) {
                        ns.scp(["scripts/deploy.js", "scripts/grow.js", "scripts/hack.js", "scripts/weak.js"], servers[i]);
                        ns.exec("scripts/deploy.js", servers[i]);
                    }
                }
            }
        }

        ns.clearLog();
        ns.disableLog("ALL");

        let maxLength = 45;
        let fString = `${queueplace} OF ${targets.length}`;
        let sString = lastaction;
        let dif2 = maxLength - fString.length;
        let dif3 = maxLength - sString.length;
        for (let i = 0; i < dif2; i++) { fString = fString + " "; }
        for (let i = 0; i < dif3; i++) { sString = sString + " "; }

        ns.print("╔════════════════════════════════════════════════════════════╗");
        ns.print(`║ TARGET        ${fString}║`);
        ns.print("╠════════════════════════════════════════════════════════════╣");
        ns.print(`║ LAST ACTION ► ${sString}║`);
        ns.print("╠════════════════════════════════════════════════════════════╣");

        let linewidth = 19.6;
        let lines = linewidth + 6 + 100;

        let index = 0;
        let lengths = [];
        let longest = 0;

        for (let i = 0; i < servers.length; i++) {
            if (ns.hasRootAccess(servers[i]) == true) {
                if (ns.getServerMaxRam(servers[i]) != 0) {
                    let namelength = `${servers[i]} [${ns.getServerMaxRam(servers[i])}]`;
                    lengths.push(namelength.length);
                }
            }
        }
        for (let i = 0; i < lengths.length; i++) {
            if (lengths[i] > longest) {
                longest = lengths[i];
            }
        }
        for (let i = 0; i < servers.length; i++) {
            if (ns.hasRootAccess(servers[i]) == true) {
                if (ns.getServerMaxRam(servers[i]) != 0) {
                    let log = JSON.parse(ns.read("logs/" + servers[i] + "-log-v2.txt"));
                    let namelength = `${servers[i]} [${ns.getServerMaxRam(servers[i])}]`;
                    index++;
                    let index2 = index
                    if (index <= 9) {
                        index2 = "0" + index;
                    } else {
                        index2 = index;
                    }
                    let snamel = namelength.length;
                    let dif = longest - snamel;
                    let output = namelength;
                    for (let i = 0; i < dif; i++) {
                        output = output + " ";
                    }
                    let color = "reset";
                    if (log[0] == "HACK") {
                        color = "red";
                    } else if (log[0] == "GROW") {
                        color = "green";
                    } else if (log[0] == "WEAK") {
                        color = "yellow";
                    }

                    let mLen = longest + 4;
                    let targ = log[1];
                    let dif4 = mLen - targ.length;

                    for (let i = 0; i < dif4; i++) {
                        targ = targ + " ";
                    }

                    ns.print(`║ (${index2}) ${output} ► ${colors[color]}${log[0]}${colors["reset"]} ► ${targ}║`);
                    lines = lines + linewidth;
                }
            }
        }
        ns.print("╚════════════════════════════════════════════════════════════╝");
        ns.resizeTail(605, lines)
        await ns.sleep(0);
        await ns.sleep(100);
    }
}

function getFreeMemory(ns, server, scriptmem) {
    let freemem = ns.getServerMaxRam(server) - ns.getServerUsedRam(server);
    let inst = Math.round(freemem / scriptmem);
    let inst2 = 0;
    if (inst > 1) {
        inst2 = inst - 1;
    } else {
        inst2 = inst;
    }
    return inst2;
}
