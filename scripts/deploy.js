/** @param {NS} ns */
export async function main(ns) {
    let hostname = ns.getHostname();
    let log = ["IDLE", "NONE"];
    ns.write("hostname.txt", hostname, "w");
    ns.write("logs/" + hostname + "-log-v2.txt", JSON.stringify(log), "w");
    ns.scp("logs/" + hostname + "-log-v2.txt", "home");
}