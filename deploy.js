/** @param {NS} ns */
export async function main(ns) {
    let servers = JSON.parse(ns.read("servers.txt"));
    for (let i = 0; i < servers.length; i++) {
        if (ns.hasRootAccess(servers[i]) == true) {
            ns.tprint(" > " + servers[i]);
            ns.scriptKill("scripts/deploy.js", servers[i]);
            ns.scriptKill("scripts/grow.js", servers[i]);
            ns.scriptKill("scripts/hack.js", servers[i]);
            ns.scriptKill("scripts/weak.js", servers[i]);
            ns.scp(["scripts/deploy.js", "scripts/grow.js", "scripts/hack.js", "scripts/weak.js"], servers[i]);
            ns.exec("scripts/deploy.js", servers[i]);
        }
    }
}