/** @param {NS} ns */
export async function main(ns) {
    while (true) {

        let servers_scan = multiscan(ns, "home");
        var unique = servers_scan.filter(function (item) {
            return item != "home";
        })
        ns.write("servers.txt", JSON.stringify(unique), "w");

        let servers_money = [];
        let targets = JSON.parse(ns.read("servers.txt"));
        for (let i = 0; i < targets.length; i++) {
            if (ns.getHackingLevel() >= ns.getServerRequiredHackingLevel(targets[i])) {
                if (ns.hasRootAccess(targets[i]) == true) {
                    let mx = ns.getServerMaxMoney(targets[i]);
                    if (mx != 0) {
                        let wr = ns.getWeakenTime(targets[i]);
                        if (wr < 200000) {
                            servers_money.push(targets[i]);
                        }
                    }
                }
            }
        }
        ns.write("hackservers.txt", JSON.stringify(servers_money), "w");

        let ports = 0;
        let maxthreads = ns.args[0];
        let servers = JSON.parse(ns.read("servers.txt"));
        let gethostname = ns.getHostname();
        let gethackingLevel = ns.getHackingLevel();
        for (let i = 0; i < servers.length; i++) {
            if (servers[i] != "home" && servers[i] != gethostname) {
                if (gethackingLevel >= ns.getServerRequiredHackingLevel(servers[i])) {
                    if (ns.hasRootAccess(servers[i]) != true) {
                        if (ns.fileExists("BruteSSH.exe") == true) {
                            ns.brutessh(servers[i], { threads: maxthreads });
                            ports++;
                        }
                        if (ns.fileExists("FTPCrack.exe") == true) {
                            ns.ftpcrack(servers[i], { threads: maxthreads });
                            ports++;
                        }
                        if (ns.fileExists("relaySMTP.exe") == true) {
                            ns.relaysmtp(servers[i], { threads: maxthreads });
                            ports++;
                        }
                        if (ns.fileExists("httpWorm.exe") == true) {
                            ns.httpworm(servers[i], { threads: maxthreads });
                            ports++;
                        }
                        if (ns.fileExists("SQLInject.exe") == true) {
                            ns.sqlinject(servers[i], { threads: maxthreads });
                            ports++;
                        }
                        try {
                            if (ns.fileExists("NUKE.exe") == true) {
                                if (ports >= ns.getServerNumPortsRequired(servers[i])) {
                                    ns.nuke(servers[i], { threads: maxthreads });
                                }
                            }
                        } catch (error) {}
                    }
                }
            }
        }
        await ns.sleep(10000);
    }
}

function multiscan(ns, server) {
    let serverlist = [];
    function scanning(server) {
        let currentscan = ns.scan(server);
        //ns.tprint("SCAN: " + server);
        currentscan.forEach(server => {
            if (!serverlist.includes(server)) {
                serverlist.push(server);
                //ns.print(" > " + server);
                scanning(server);
            }
        })
    }
    scanning(server);
    return serverlist;
}