/** @param {NS} ns */
export async function main(ns) {
    let hostname = ns.read("hostname.txt");
    let maxthreads = ns.args[0];
    let target = ns.args[1];
    let log = ["WEAK", target];
    ns.write("logs/" + hostname + "-log-v2.txt", JSON.stringify(log), "w");
    ns.scp("logs/" + hostname + "-log-v2.txt", "home");
    await ns.weaken(target, { threads: maxthreads });
    log = ["IDLE", "NONE"];
    ns.write("logs/" + hostname + "-log-v2.txt", JSON.stringify(log), "w");
    ns.scp("logs/" + hostname + "-log-v2.txt", "home");
}